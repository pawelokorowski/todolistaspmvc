using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using ToDoApplication.Models;
using ToDoApplication.Models.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<ToDoListDbContext>(opts =>
{
    opts.UseSqlServer(builder.Configuration["ConnectionStrings:ToDoListAppConnection"]);
});

builder.Services.AddScoped<IToDoListRepository, EFToDoListRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");

    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseAuthorization();

app.MapControllerRoute(
    name: "pagination",
    pattern: "ToDoLists/Page{ToDoListPage:int}",
    defaults: new { Controller = "Home", action = "Index", toDoListPage = 1 });

app.MapControllerRoute(
    name: "toDoListStatusPage",
    pattern: "{toDoListStatus}/Page{toDoListPage:int}",
    defaults: new { Controller = "Home", action = "Index" });
  
app.MapControllerRoute(
   name: "toDoListStatus",
   pattern: "ToDoLists/{toDoListStatus}",
   defaults: new { Controller = "Home", action = "Index", toDoListPage = 1 });

app.MapControllerRoute(
   name: "CreateList",
   pattern: "ToDoList/Create",
   defaults: new { Controller = "ToDoLists", action = "Create" });

app.MapControllerRoute(
   name: "CreateToDoItem",
   pattern: "ToDoItems/AddTask/{listId:long}",
   defaults: new { Controller = "ToDoItems", action = "Create" });

app.MapControllerRoute(
   name: "CreateToDoNote",
   pattern: "ToDoItemNotes/AddNote/{itemId:long}",
   defaults: new { Controller = "ToDoItemNotes", action = "Create" });

app.MapControllerRoute(
   name: "DueTasks",
   pattern: "DueTasks",
   defaults: new { Controller = "ToDoItems", action = "Index" });

app.MapControllerRoute(
    name: "default",
    pattern: "/",
    defaults: new { Controller = "Home", action = "Index" });

app.MapDefaultControllerRoute();

SeedData.EnsurePopulated(app);

app.Run();
