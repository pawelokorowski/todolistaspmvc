﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;
using ToDoApplication.Models.Repositories;

namespace ToDoApplication.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
       public IViewComponentResult Invoke()
       {
            List<string> toDoListStatuses = new List<string> { "Active", "Hidden", "All" };

            this.ViewBag.SelectedToDoListStatus = this.RouteData?.Values["toDoListStatus"];

            return this.View(toDoListStatuses);
       }
    }
}
