﻿namespace ToDoApplication.Models.ViewModels
{
    public class ToDoListListViewModel
    {
        public IEnumerable<ToDoList> ToDoLists { get; set; } = Enumerable.Empty<ToDoList>();

        public PagingInfo PagingInfo { get; set; } = new();

        public string CurrentToDoListStatus { get; set; } = "Active";
    }
}
