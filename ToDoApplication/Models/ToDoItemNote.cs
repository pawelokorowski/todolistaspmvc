﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApplication.Models
{
    public class ToDoItemNote
    {
        public long ToDoItemNoteId { get; set; }

        [Required]
        public string NoteContent { get; set; } = string.Empty;

        public long ToDoItemId { get; set; }

        public ToDoItem? ToDoItem { get; set; }
    }
}
