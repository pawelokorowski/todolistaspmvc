﻿using Microsoft.EntityFrameworkCore;

namespace ToDoApplication.Models
{
    public class ToDoListDbContext : DbContext
    {
        public ToDoListDbContext(DbContextOptions<ToDoListDbContext> options)
            : base(options)
        {
        }

        public DbSet<ToDoList> ToDoLists { get; set; }

        public DbSet<ToDoItem> ToDoItems { get; set; }

        public DbSet<ToDoItemNote> ToDoItemNotes { get; set; }
    }
}
