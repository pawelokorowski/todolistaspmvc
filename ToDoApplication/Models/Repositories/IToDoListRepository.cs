﻿namespace ToDoApplication.Models.Repositories
{
    public interface IToDoListRepository
    {
        IQueryable<ToDoList> ToDoLists { get; }

        IQueryable<ToDoItem> ToDoItems { get; }
    }
}
