﻿namespace ToDoApplication.Models.Repositories
{
    public class EFToDoListRepository : IToDoListRepository
    {
        private readonly ToDoListDbContext context;

        public EFToDoListRepository(ToDoListDbContext context)
        {
            this.context = context;
        }

        public IQueryable<ToDoItemNote> ToDoItemNotes => this.context.ToDoItemNotes;

        public IQueryable<ToDoItem> ToDoItems => this.context.ToDoItems;

        public IQueryable<ToDoList> ToDoLists => this.context.ToDoLists;
    }
}
