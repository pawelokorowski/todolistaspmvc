﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoApplication.Models
{
    public class ToDoItem
    {
        public long ToDoItemId { get; set; }

        [Required]
        public string Title { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public ToDoItemStatus Status { get; set; } = ToDoItemStatus.NotStarted;

        public DateTime CreationDate { get; set; } = DateTime.Now;

        [Required]
        public DateTime DueDate { get; set; } = DateTime.Now;

        public ICollection<ToDoItemNote>? ToDoItemNotes { get; set; }

        public long ToDoListId { get; set; }

        public ToDoList? ToDoList { get; set; }
    }
}
