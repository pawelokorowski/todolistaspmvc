﻿namespace ToDoApplication.Models
{
    public enum ToDoListVisibility
    {
        Hidden,
        Active, 
        All
    }
}
