﻿using System.ComponentModel.DataAnnotations;

namespace ToDoApplication.Models
{
    public class ToDoList
    {
        public long ToDoListId { get; set; }

        [RegularExpression(@"^.{1,}$", ErrorMessage = "Minimum 1 character required")]
        [Required(ErrorMessage = "ToDoListTitle is Required")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Maximum 100 characters")]
        public string ToDoListTitle { get; set; } = string.Empty;

        public bool IsVisible { get; set; } = true;

        public ICollection<ToDoItem>? ToDoItems { get; set; }
    }
}
