﻿namespace ToDoApplication.Models
{
    public enum ToDoItemStatus
    {
        Completed,
        InProgress, 
        NotStarted,
    }
}
