﻿using Microsoft.EntityFrameworkCore;

namespace ToDoApplication.Models
{
    public class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ToDoListDbContext context = app.ApplicationServices
                        .CreateScope().ServiceProvider.GetRequiredService<ToDoListDbContext>();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            if (!context.ToDoLists.Any())
            {
                context.ToDoLists.AddRange(
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList1",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList2",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList3",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList4",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList5",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList6",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList8",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList9",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList10",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList11",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Benchpress",
                                Description = "Muscle",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "Time to get swolle",
                                    },
                                },
                            },
                        },
                    });
                context.SaveChanges();
            }
        }
    }
}
