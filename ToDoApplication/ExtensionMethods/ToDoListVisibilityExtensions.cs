﻿using ToDoApplication.Models;

namespace ToDoApplication.ExtensionMethods
{
    public static class ToDoListVisibilityExtensions
    {
        public static ToDoListVisibility StringToListVisibillity(this string toDoListVisibility)
        {
            ToDoListVisibility toDoListVisibilityEnum;

            if (Enum.TryParse<ToDoListVisibility>(toDoListVisibility, out toDoListVisibilityEnum))
            {
                return toDoListVisibilityEnum;
            }
            else
            {
                return ToDoListVisibility.Active;
            }
        }

        public static string StringToListVisibillity(this bool toDoListVisibility)
        {

            if (toDoListVisibility)
            {
                return "Active";
            }
            else
            {
                return "InActive";
            }
        }
    }
}
