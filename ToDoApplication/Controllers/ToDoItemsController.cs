﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ToDoApplication.Models;

namespace ToDoApplication.Controllers
{
    public class ToDoItemsController : Controller
    {
        private readonly ToDoListDbContext context;

        public ToDoItemsController(ToDoListDbContext context)
        {
            this.context = context;
        }

        // GET: ToDoItems
        public async Task<IActionResult> Index()
        {
            var toDoListDbContext = this.context.ToDoItems.Include(t => t.ToDoList).Where(t => t.DueDate.Date == DateTime.Today && t.ToDoList!.IsVisible && t.Status != ToDoItemStatus.Completed);
            return this.View(await toDoListDbContext.ToListAsync());
        }

        // GET: ToDoItems/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null || this.context.ToDoItems == null)
            {
                return this.NotFound();
            }

            var toDoItem = await this.context.ToDoItems
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.ToDoItemId == id);
            if (toDoItem == null)
            {
                return this.NotFound();
            }

            return this.View(toDoItem);
        }

        // GET: ToDoItems/Create

        public ViewResult Create(long listId)
        {
            ToDoItem toDoItem = new ToDoItem() { ToDoListId = listId };
            return this.View(toDoItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create2([Bind("ToDoItemId,Title,Description,Status,CreationDate,DueDate,ToDoListId")] ToDoItem toDoItem)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }
            else
            {
                this.context.Add(toDoItem);
                await this.context.SaveChangesAsync();
                return this.RedirectToAction("Tasks", "Home", new { id = toDoItem.ToDoListId });
            }
        }

        // GET: ToDoItems/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || this.context.ToDoItems == null)
            {
                return this.NotFound();
            }

            var toDoItem = await this.context.ToDoItems.FindAsync(id);
            if (toDoItem == null)
            {
                return this.NotFound();
            }

            this.ViewData["ToDoListId"] = new SelectList(this.context.ToDoLists, "ToDoListId", "ToDoListId", toDoItem.ToDoListId);
            return this.View(toDoItem);
        }

        // POST: ToDoItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ToDoItemId,Title,Description,Status,CreationDate,DueDate,ToDoListId")] ToDoItem toDoItem)
        {
            if (id != toDoItem.ToDoItemId)
            {
                return this.NotFound();
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    this.context.Update(toDoItem);
                    await this.context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.ToDoItemExists(toDoItem.ToDoItemId))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.RedirectToAction("Tasks", "Home", new { id = toDoItem.ToDoListId });
            }

            this.ViewData["ToDoListId"] = new SelectList(this.context.ToDoLists, "ToDoListId", "ToDoListId", toDoItem.ToDoListId);
            return this.View(toDoItem);
        }

        // GET: ToDoItems/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null || this.context.ToDoItems == null)
            {
                return this.NotFound();
            }

            var toDoItem = await this.context.ToDoItems
                .Include(t => t.ToDoList)
                .FirstOrDefaultAsync(m => m.ToDoItemId == id);
            if (toDoItem == null)
            {
                return this.NotFound();
            }

            return this.View(toDoItem);
        }

        // POST: ToDoItems/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            if (this.context.ToDoItems == null)
            {
                return this.Problem("Entity set 'ToDoListDbContext.ToDoItems'  is null.");
            }

            var toDoItem = await this.context.ToDoItems.FindAsync(id);
            if (toDoItem != null)
            {
                this.context.ToDoItems.Remove(toDoItem);
            }
            
            await this.context.SaveChangesAsync();
            return this.RedirectToAction("Tasks", "Home", new { id = toDoItem!.ToDoListId });
        }

        private bool ToDoItemExists(long id)
        {
          return this.context.ToDoItems.Any(e => e.ToDoItemId == id);
        }
    }
}
