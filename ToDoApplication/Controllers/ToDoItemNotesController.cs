﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ToDoApplication.Models;

namespace ToDoApplication.Controllers
{
    public class ToDoItemNotesController : Controller
    {
        private readonly ToDoListDbContext context;

        public ToDoItemNotesController(ToDoListDbContext context)
        {
            this.context = context;
        }

        public async Task<IActionResult> TaskNotes(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var toDoItem = await this.context.ToDoItems
                .Include(s => s.ToDoItemNotes)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.ToDoItemId == id);

            if (toDoItem == null)
            {
                return this.NotFound();
            }

            return this.View(toDoItem);
        }

        // GET: ToDoItemNotes/Create
        public IActionResult Create(long itemId)
        {
            ToDoItemNote toDoItemNote = new ToDoItemNote() { ToDoItemId = itemId };
            return this.View(toDoItemNote);
        }

        // POST: ToDoItemNotes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create2([Bind("ToDoItemNoteId,NoteContent,ToDoItemId")] ToDoItemNote toDoItemNote)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }
            else
            {
                this.context.Add(toDoItemNote);
                await this.context.SaveChangesAsync();
                return this.RedirectToAction("TaskNotes", "ToDoItemNotes", new { id = toDoItemNote.ToDoItemId });
            }
        }

        // GET: ToDoItemNotes/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || this.context.ToDoItemNotes == null)
            {
                return this.NotFound();
            }

            var toDoItemNote = await this.context.ToDoItemNotes.FindAsync(id);
            if (toDoItemNote == null)
            {
                return this.NotFound();
            }
            this.ViewData["ToDoItemId"] = new SelectList(this.context.ToDoItems, "ToDoItemId", "ToDoItemId", toDoItemNote.ToDoItemId);
            return this.View(toDoItemNote);
        }

        // POST: ToDoItemNotes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ToDoItemNoteId,NoteContent,ToDoItemId")] ToDoItemNote toDoItemNote)
        {
            if (id != toDoItemNote.ToDoItemNoteId)
            {
                return this.NotFound();
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    this.context.Update(toDoItemNote);
                    await this.context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.ToDoItemNoteExists(toDoItemNote.ToDoItemNoteId))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.RedirectToAction("TaskNotes", "ToDoItemNotes", new { id = id });
            }

            this.ViewData["ToDoItemId"] = new SelectList(this.context.ToDoItems, "ToDoItemId", "ToDoItemId", toDoItemNote.ToDoItemId);
            return View(toDoItemNote);
        }

        // GET: ToDoItemNotes/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null || this.context.ToDoItemNotes == null)
            {
                return this.NotFound();
            }

            var toDoItemNote = await this.context.ToDoItemNotes
                .Include(t => t.ToDoItem)
                .FirstOrDefaultAsync(m => m.ToDoItemNoteId == id);
            if (toDoItemNote == null)
            {
                return this.NotFound();
            }

            return this.View(toDoItemNote);
        }

        // POST: ToDoItemNotes/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            if (this.context.ToDoItemNotes == null)
            {
                return this.Problem("Entity set 'ToDoListDbContext.ToDoItemNotes'  is null.");
            }

            var toDoItemNote = await this.context.ToDoItemNotes.FindAsync(id);
            if (toDoItemNote != null)
            {
                this.context.ToDoItemNotes.Remove(toDoItemNote);
            }
            
            await this.context.SaveChangesAsync();
            return this.RedirectToAction("TaskNotes", "ToDoItemNotes", new { id = id });
        }

        private bool ToDoItemNoteExists(long id)
        {
          return this.context.ToDoItemNotes.Any(e => e.ToDoItemNoteId == id);
        }
    }
}
