﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using ToDoApplication.ExtensionMethods;
using ToDoApplication.Models;
using ToDoApplication.Models.Repositories;
using ToDoApplication.Models.ViewModels;

namespace ToDoApplication.Controllers
{
    public class HomeController : Controller
    { 
        private readonly IToDoListRepository repository;

        public HomeController(IToDoListRepository repository)
        {
            this.repository = repository;
        }

        public int PageSize { get; set; } = 4;

        public ViewResult Index(string toDoListStatus = "Active", int toDoListPage = 1)
        {
            IEnumerable<ToDoList> toDoLists;
            toDoLists = this.repository.ToDoLists;

            if(toDoListStatus.StringToListVisibillity() == ToDoListVisibility.Active)
            {
                toDoLists = toDoLists.Where(toDoList => toDoList.IsVisible);
            }

            if (toDoListStatus.StringToListVisibillity() == ToDoListVisibility.Hidden)
            {
                toDoLists = toDoLists.Where(toDoList => !toDoList.IsVisible);
            }

            int toDoListsPageCount = toDoLists.Count();

            return this.View(new ToDoListListViewModel
            {
                ToDoLists = toDoLists
                              .OrderBy(p => p.ToDoListId)
                              .Skip((toDoListPage - 1) * this.PageSize)
                              .Take(this.PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = toDoListPage,
                    ItemsPerPage = this.PageSize,
                    TotalItems = toDoListStatus == null ? this.repository.ToDoLists.Count() : toDoListsPageCount,
                },
                CurrentToDoListStatus = toDoListStatus!,
            });
        }

        public async Task<IActionResult> Tasks(int? id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var toDoList = await this.repository.ToDoLists
                .Include(s => s.ToDoItems)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.ToDoListId == id);

            if (toDoList == null)
            {
                return this.NotFound();
            }

            return this.View(toDoList);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }
    }
}