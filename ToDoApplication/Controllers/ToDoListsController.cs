﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ToDoApplication.Models;

namespace ToDoApplication.Controllers
{
    public class ToDoListsController : Controller
    {
        private readonly ToDoListDbContext context;

        public ToDoListsController(ToDoListDbContext context)
        {
            this.context = context;
        }

        // GET: ToDoLists/Create
        public IActionResult Create()
        {
            return this.View();
        }

        // POST: ToDoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ToDoListId,ToDoListTitle,IsVisible")] ToDoList toDoList)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }
            else
            {
                this.context.Add(toDoList);
                await this.context.SaveChangesAsync();
                return this.RedirectToAction("Index", "Home");
            }
        }

        // GET: ToDoLists/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null || this.context.ToDoLists == null)
            {
                return this.NotFound();
            }

            var toDoList = await this.context.ToDoLists.FindAsync(id);
            if (toDoList == null)
            {
                return this.NotFound();
            }

            return this.View(toDoList);
        }

        // POST: ToDoLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("ToDoListId,ToDoListTitle,IsVisible")] ToDoList toDoList)
        {
            if (id != toDoList.ToDoListId)
            {
                return this.NotFound();
            }

            if (this.ModelState.IsValid)
            {
                try
                {
                    this.context.Update(toDoList);
                    await this.context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!this.ToDoListExists(toDoList.ToDoListId))
                    {
                        return this.NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return this.RedirectToAction("Index", "Home");
            }

            return this.View(toDoList);
        }

        // GET: ToDoLists/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null || this.context.ToDoLists == null)
            {
                return this.NotFound();
            }

            var toDoList = await this.context.ToDoLists
                .FirstOrDefaultAsync(m => m.ToDoListId == id);
            if (toDoList == null)
            {
                return this.NotFound();
            }

            return this.View(toDoList);
        }

        // POST: ToDoLists/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            if (this.context.ToDoLists == null)
            {
                return this.Problem("Entity set 'ToDoListDbContext.ToDoLists'  is null.");
            }

            var toDoList = await this.context.ToDoLists.FindAsync(id);
            if (toDoList != null)
            {
                this.context.ToDoLists.Remove(toDoList);
            }
            
            await this.context.SaveChangesAsync();
            return this.RedirectToAction("Index", "Home");
        }

        private bool ToDoListExists(long id)
        {
          return this.context.ToDoLists.Any(e => e.ToDoListId == id);
        }
    }
}
