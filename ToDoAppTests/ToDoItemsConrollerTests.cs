using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoApplication.Controllers;
using ToDoApplication.Models;
using ToDoApplication.Models.Repositories;
using ToDoApplication.Models.ViewModels;

namespace ToDoAppTests
{
    public class ToDoItemsConrollerTests
    {

        private readonly ToDoListDbContext context;

        /// <summary>
        /// Method initilizes user context for testing.
        /// </summary>
        public ToDoItemsConrollerTests()
        {
            this.context = ContextGenerator.GetInstance("ToDoItemsControllerTests");
        }

        [Fact]
        public async Task ToDoItemsController_Index_GetAction_MustReturnViewResult()
        {
            ToDoItemsController toDoItemsController = new(context);
                        var result = await toDoItemsController.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.IsAssignableFrom<IEnumerable<ToDoApplication.Models.ToDoItem>>(viewResult.Model);
        }

        [Fact]
        public async Task ToDoItemsController_Index_GetAction_MustReturnViewWithToDoLists()
        {
            ToDoItemsController toDoItemsController = new(context);
            var result = await toDoItemsController.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var viewModel = Assert.IsAssignableFrom<IEnumerable<ToDoItem>>(viewResult.ViewData.Model);

            Assert.Equal(2, viewModel.Count());
        }

        [Fact]
        public async Task ToDoItemsController_Create2_ReturnsBadRequest_GivenInvalidModel()
        {
            // Arrange & Act
            ToDoItemsController toDoItemsController = new(context);

            toDoItemsController.ModelState.AddModelError("error", "some error");

            ToDoItem item = new();
            // Act
            var result = await toDoItemsController.Create2(toDoItem: item);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ToDoItemsController_Create2_Redirects_To_Action_For_validModel()
        {
            // Arrange & Act
            ToDoItemsController toDoItemsController = new(context);



            ToDoItem item = new ToDoItem
            {
                Title = "Morning Jogging",
                Description = "I could use some exercise",
                Status = ToDoItemStatus.InProgress,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now,
                ToDoListId = 1,
                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "Note 2",
                                    },
                                },
            };

            var result = await toDoItemsController.Create2(toDoItem: item);

            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Home", redirectToActionResult.ControllerName);

            Assert.Equal("Tasks", redirectToActionResult.ActionName);

        }
    }
}