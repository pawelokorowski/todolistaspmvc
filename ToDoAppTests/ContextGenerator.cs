﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Models;

namespace ToDoAppTests
{
    public static class ContextGenerator
    {
        public static ToDoListDbContext GetInstance(string databaseName)
        {
            var options = new DbContextOptionsBuilder<ToDoListDbContext>()
                      .UseInMemoryDatabase(databaseName: databaseName)
                      .Options;

            var instaceContext = new ToDoListDbContext(options);
            instaceContext.Database.EnsureDeleted();

            instaceContext.ToDoLists.AddRange(
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList1",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.InProgress,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now,
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "Note 2",
                                    },
                                },
                            },
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.Completed,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now,
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "Note 2",
                                    },
                                },
                            },
                        },

                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList2",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now,
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList3",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList4",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList5",
                        IsVisible = true,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList6",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList7",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList8",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList9",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList10",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Jogging",
                                Description = "I could use some exercise",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
                            },
                        },
                    },
                    new ToDoList
                    {
                        ToDoListTitle = "MyToDoList11",
                        IsVisible = false,
                        ToDoItems = new List<ToDoItem>()
                        {
                            new ToDoItem
                            {
                                Title = "Morning Benchpress",
                                Description = "Muscle",
                                Status = ToDoItemStatus.NotStarted,
                                CreationDate = DateTime.Now,
                                DueDate = DateTime.Now.AddDays(1),
                                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "Time to get swolle",
                                    },
                                },
                            },
                        },
                    });
            instaceContext.SaveChanges();
            instaceContext.Database.EnsureCreated();

            return instaceContext;
        }

        public static ToDoListDbContext GetEmptyInstance(string databaseName)
        {
            var options = new DbContextOptionsBuilder<ToDoListDbContext>()
                      .UseInMemoryDatabase(databaseName: databaseName)
                      .Options;

            var instaceContext = new ToDoListDbContext(options);
            instaceContext.Database.EnsureDeleted();
            instaceContext.Database.EnsureCreated();

            return instaceContext;
        }
    }
}
