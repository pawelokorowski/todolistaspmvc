﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Controllers;
using ToDoApplication.Models;

namespace ToDoAppTests
{
    public class ModelValidationTests
    {
        [Fact]
        public void Correct_ToDoItem_Validates()
        {
            var obj = new ToDoItem
            {
                Title = "Morning Jogging",
                Description = "I could use some exercise",
                Status = ToDoItemStatus.NotStarted,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(1),
                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
            };
            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateObject(obj, context, results, true);

            Assert.True(valid);
        }

        [Fact]
        public void ToDoItem_Requires_Title()
        {
            var obj = new ToDoItem
            {
                Description = "I could use some exercise",
                Status = ToDoItemStatus.NotStarted,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(1),
                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
            };
            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateObject(obj, context, results, true);

            Assert.True(!valid);
        }

        [Fact]
        public void ToDoItem_Title_Can_Not_Be_Empty()
        {
            var obj = new ToDoItem
            {
                Title = "",
                Description = "I could use some exercise",
                Status = ToDoItemStatus.NotStarted,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(1),
                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
            };
            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateObject(obj, context, results, true);

            Assert.True(!valid);
        }

        [Fact]
        public void ToDoItem_Title_Can_Not_Be_Longer_Than_100()
        {
            var obj = new ToDoItem
            {
                Title = "",
                Description = new string('a', 101),
                Status = ToDoItemStatus.NotStarted,
                CreationDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(1),
                ToDoItemNotes = new List<ToDoItemNote>()
                                {
                                    new ToDoItemNote()
                                    {
                                        NoteContent = "I should ask mary if she wants to run with me",
                                    },
                                },
            };

            var context = new ValidationContext(obj);
            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateObject(obj, context, results, true);

            Assert.True(!valid);
        }

    }
}
