﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Controllers;
using ToDoApplication.Models;

namespace ToDoAppTests
{
    public class ToDoListsControllerTests
    {
        private readonly ToDoListDbContext context;

        /// <summary>
        /// Method initilizes user context for testing.
        /// </summary>
        public ToDoListsControllerTests()
        {
            this.context = ContextGenerator.GetInstance("ToDoListsControllerTests");
        }

        [Fact]
        public async Task ToDoListsController_Create_ReturnsBadRequest_GivenInvalidModel()
        {
            // Arrange & Act
            ToDoListsController toDoListsController = new(context);

            toDoListsController.ModelState.AddModelError("error", "some error");

            ToDoList list = new();
            // Act
            var result = await toDoListsController.Create(toDoList: list);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ToDoListsController_Create_Redirects_To_Action_For_validModel()
        {
            // Arrange & Act
            ToDoListsController toDoListsController = new(context);



            ToDoList list = new()
            {
                ToDoListTitle = "MyToDoList1",
                IsVisible = true,
            };

            var result = await toDoListsController.Create(toDoList: list);

            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Home", redirectToActionResult.ControllerName);

            Assert.Equal("Index", redirectToActionResult.ActionName);

        }

    }
}
