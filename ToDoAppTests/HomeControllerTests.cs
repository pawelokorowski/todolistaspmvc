﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ToDoApplication.Controllers;
using ToDoApplication.Models;
using ToDoApplication.Models.Repositories;

namespace ToDoAppTests
{
    public class HomeControllerTests
    {
        private readonly IToDoListRepository context;


        /// <summary>
        /// Method initilizes user context for testing.
        /// </summary>
        public HomeControllerTests()
        {
            this.context = new EFToDoListRepository( ContextGenerator.GetInstance("HomeControllerTests"));
        }

        [Fact]
        public void Home_Index_GetAction_MustReturnViewResult()
        {
            HomeController homeController = new(context);
            var result = homeController.Index();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async Task Home_Tasks_GetAction_MustReturnViewWithToDoitems_If_List_With_Specific_Id_Exists()
        {
            HomeController toDoItemsController = new(context);
            var result = await toDoItemsController.Tasks(1);

            var viewResult = Assert.IsType<ViewResult>(result);
            var viewModel = Assert.IsAssignableFrom<ToDoList>(viewResult.ViewData.Model);

            Assert.Equal(2, viewModel.ToDoItems!.Count);
        }

        [Fact]
        public async Task Home_Tasks_GetAction_Returns_NotFoundResult_For_Not_Existing_ToDoList()
        {
            HomeController toDoItemsController = new(context);
            var result = await toDoItemsController.Tasks(1000);
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task Home_Tasks_GetAction_Returns_NotFoundResult_For_Null_Id()
        {
            HomeController toDoItemsController = new(context);
            var result = await toDoItemsController.Tasks(null);
            Assert.IsType<NotFoundResult>(result);
        }

    }
}
