﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Controllers;
using ToDoApplication.Models;

namespace ToDoAppTests
{
    public class ToDoItemNotesControllerTests
    {
        private readonly ToDoListDbContext context;

        public ToDoItemNotesControllerTests()
        {
            this.context = ContextGenerator.GetInstance("ToDoItemNotesControllerTests");
        }

        [Fact]
        public async Task ToDoItemNotesController_Create2_ReturnsBadRequest_GivenInvalidModel()
        {
            // Arrange & Act
            ToDoItemNotesController toDoItemNotesController = new(context);

            toDoItemNotesController.ModelState.AddModelError("error", "some error");

            ToDoItemNote note = new();
            // Act
            var result = await toDoItemNotesController.Create2(toDoItemNote: note);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ToDoItemsController_Create2_Redirects_To_Action_For_validModel()
        {
            // Arrange & Act
            ToDoItemNotesController toDoItemNotesController = new(context);



            ToDoItemNote note = new ToDoItemNote
            {
                NoteContent = "RFEWFWE",
                ToDoItemId = 1 
            };

            var result = await toDoItemNotesController.Create2(toDoItemNote: note);

            var redirectToActionResult = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("ToDoItemNotes", redirectToActionResult.ControllerName);

            Assert.Equal("TaskNotes", redirectToActionResult.ActionName);
        }

        [Fact]
        public async Task ToDoItemsController_Create2_Adds_New_ToDoItemNote_To_Context()
        {
            int numberOfNotes = context!.ToDoItemNotes.Count();

            ToDoItemNotesController toDoItemNotesController = new(context);



            ToDoItemNote note = new ToDoItemNote
            {
                NoteContent = "RFEWFWE",
                ToDoItemId = 1
            };

            await toDoItemNotesController.Create2(toDoItemNote: note);

            int numberOfNotesAfterAdd = context!.ToDoItemNotes.Count();


            Assert.Equal(numberOfNotes + 1, numberOfNotesAfterAdd);
        }
    }
}
